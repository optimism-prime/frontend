# Optimism Prime Frontend

This is the main repository for the [Optimism Prime frontend](https://www.optimismprime.io/).

## Local development setup

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### Dependencies

Dependencies are handled with [`pnpm`](https://pnpm.io/). Install it first.

### Getting Started

First, run the development server:

```bash
pnpm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

### Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Official Deployment

The code is deployed on https://www.optimismprime.io/ by pushing to the `release` branch of https://gitlab.com/c2ba/optimism-prime-frontend-deploy-fork

The deployment is done on a fork repository because Vercel free plan only allows to deploy repositories at the root of an account, while https://gitlab.com/optimism-prime/frontend is in a group.

## How to contribute

Here is the workflow to collaborate on this project:

- Join [our discord](https://discord.gg/m3Na57wfq4) and keep us in touch in the [builders channel](https://discord.com/channels/1015287469064867873/1044676633803624558)
- Create a fork
- Start an issue to describe what you want to do
- Submit a merge request from your fork to this repository
- Edit the merge request description with "Closes #x" where `x` is the id of the issue
- Prefix the MR title with `Draft: ` while it's a work in progress
- When you think it's ready to be merged, be sure the commit history is clean using history rewrite techniques like interactive rebase. Here are some tutorials:
  - [How to keep your Git history clean with interactive rebase | GitLab](https://about.gitlab.com/blog/2020/11/23/keep-git-history-clean-with-interactive-rebase/)
  - [Rebase Rewriting History - Mastering Git - raywenderlich.com - YouTube](https://www.youtube.com/watch?v=9y9IRBKDU4I)
- Remove the `Draft: ` prefix
- Ask for a review on discord

