import { BigNumber, ethers } from "ethers";
import { Dispatch, SetStateAction, useCallback, useContext, useEffect, useMemo, useState } from "react";
import { Block, Button, ExternalLink, Modal } from "../components/ui";
import { StateContext } from "../context/StateContext";
import { approve, claimRelicRewards, createRelicAndDeposit, depositInRelic, getAllRelics, PoolInfo, RelicInfo, ReliquaryInfo, TokenInfo, updatePosition, withdrawFromRelic } from "../contracts/reliquary";
import notificateTransaction from "../contracts/shared/notificate-transaction";
import {
  OPP_BORROWABLE_CONTRACT,
  OPP_CONTRACT,
  VELODROMEV1_ETH_USDC_CONTRACT,
  VELODROME_OPP_ETH_CONTRACT_V1,
  VELODROME_OPP_FBOMB_CONTRACT_V1,
  VELODROME_OPP_OP_CONTRACT_V1,
  VELODROME_OPP_ETH_CONTRACT_V2,
  VELODROME_OPP_FBOMB_CONTRACT_V2,
  VELODROME_OPP_OP_CONTRACT_V2,
  EQUALIZER_FTM_USDC_CONTRACT,
  VELODROMEV2_OP_VELO_CONTRACT,
  VELODROMEV2_WETH_LUSD_CONTRACT,
  VELODROMEV2_SUSD_LUSD_CONTRACT,
  RETRO_GAMMA_WETH_USDC05_CONTRACT,
  RETRO_GAMMA_WMATIC_USDC05_CONTRACT,
  RETRO_ICHI_WETH_USDC05_CONTRACT,
  RETRO_ICHI_WMATIC_USDC05_CONTRACT,
  RETRO_GAMMA_USDC_CASH01_CONTRACT,
  VELODROMEV2_ETH_GRAIN_CONTRACT,
  VELODROME_OPP_OPXVELO_CONTRACT_V2,
  VELODROME_OPP_2192_CONTRACT_V2,
  BEETHOVENX_OPTIMISM_PRIME_SOUNDWAVE,
} from "../utils/constants";
import Notification from "./Notification";

type PoolUIInfo = {
  url: string;
  displayName: string;
  shortDisplayName: string;
};

const poolTokenToUIInfo = {
  [OPP_CONTRACT]: {
    url: "https://velodrome.finance/swap?from=eth&to=0x676f784d19c7f1ac6c6beaeaac78b02a73427852",
    displayName: "Optimism Prime $OPP",
    shortDisplayName: "OPP",
  },
  [OPP_BORROWABLE_CONTRACT]: {
    // Tarot OPP Borrowable
    url: "https://www.tarot.to/lending-pool/10/0x31ab35018a205d434c31fda7fc6cfab5d58e4ff9",
    displayName: "Tarot Borrowable $OPP",
    shortDisplayName: "Tarot OPP",
  },
  [VELODROME_OPP_ETH_CONTRACT_V1]: {
    // Velodrome OPP/ETH V1
    url: "https://v1.velodrome.finance/liquidity/manage?address=0x9e0fed4f8284b5b81601b4c7fa50f68dbf958a86",
    displayName: "Velodrome OPP/ETH",
    shortDisplayName: "OPP/ETH",
  },
  [VELODROME_OPP_FBOMB_CONTRACT_V1]: {
    // Velodrome OPP/fBOMB V1
    url: "https://v1.velodrome.finance/liquidity/manage?address=0x87bdf9ba91f353777fb1fe7cf4b7dfecf80d714e",
    displayName: "Velodrome OPP/fBOMB",
    shortDisplayName: "OPP/fBOMB",
  },
  [VELODROME_OPP_OP_CONTRACT_V1]: {
    // Velodrome OPP/OP V1
    url: "https://v1.velodrome.finance/liquidity/manage?address=0xab004e185954e84bd7bb176bf21ea09897118ddb",
    displayName: "Velodrome OPP/OP",
    shortDisplayName: "OPP/OP",
  },
  [VELODROME_OPP_ETH_CONTRACT_V2]: {
    // Velodrome OPP/ETH V2
    url: "https://velodrome.finance/deposit?token0=0x676f784d19c7f1ac6c6beaeaac78b02a73427852&token1=eth&type=-1&factory=0xF1046053aa5682b4F9a81b5481394DA16BE5FF5a",
    displayName: "Velodrome OPP/ETH",
    shortDisplayName: "OPP/ETH",
  },
  [VELODROME_OPP_FBOMB_CONTRACT_V2]: {
    // Velodrome OPP/fBOMB V2
    url: "https://velodrome.finance/deposit?token0=0x676f784d19c7f1ac6c6beaeaac78b02a73427852&token1=0x74ccbe53f77b08632ce0cb91d3a545bf6b8e0979&type=-1&factory=0xF1046053aa5682b4F9a81b5481394DA16BE5FF5a",
    displayName: "Velodrome OPP/fBOMB",
    shortDisplayName: "OPP/fBOMB 💥",
  },
  [VELODROME_OPP_OP_CONTRACT_V2]: {
    // Velodrome OPP/OP V2
    url: "https://velodrome.finance/deposit?token0=0x676f784d19c7f1ac6c6beaeaac78b02a73427852&token1=0x4200000000000000000000000000000000000042&type=-1&factory=0xF1046053aa5682b4F9a81b5481394DA16BE5FF5a",
    displayName: "Velodrome OPP/OP",
    shortDisplayName: "OPP/OP",
  },
  [VELODROME_OPP_OPXVELO_CONTRACT_V2]: {
    // Velodrome OPP/opxVELO
    url: "https://velodrome.finance/deposit?token0=0x676f784d19c7f1ac6c6beaeaac78b02a73427852&token1=0xc38464250f51123078bbd7ea574e185f6623d037&type=-1&factory=0xF1046053aa5682b4F9a81b5481394DA16BE5FF5a",
    displayName: "Velodrome OPP/opxVELO",
    shortDisplayName: "OPP/opxVELO",
  },
  [VELODROME_OPP_2192_CONTRACT_V2]: {
    // Velodrome OPP/2192
    url: "https://velodrome.finance/deposit?token0=0x676f784d19c7f1ac6c6beaeaac78b02a73427852&token1=0x3ed9acaac7bd974eb83a8ea6432a239e3c829d5d&type=-1&factory=0xF1046053aa5682b4F9a81b5481394DA16BE5FF5a",
    displayName: "Velodrome OPP/2192",
    shortDisplayName: "OPP/2192 ⚔️",
  },
  [BEETHOVENX_OPTIMISM_PRIME_SOUNDWAVE]: {
    // Beethoven X OPP/imxB-OPP-ETH/imxB-OPP-opxVELO
    url: "https://op.beets.fi/pool/0xadf86a03af1c77d81380f9fa7c4c797a3ebf2d3a000100000000000000000146",
    displayName: "BeethovenX Optimism Prime Soundwave",
    shortDisplayName: "OPP Soundwave 🎵",
  },
  [VELODROMEV1_ETH_USDC_CONTRACT]: {
    url: `https://velodrome.finance/liquidity/manage?address=0x79c912fef520be002c2b6e57ec4324e260f38e50`,
    displayName: "ETH/USDC",
    shortDisplayName: "ETH/USDC",
  },
  [VELODROMEV2_OP_VELO_CONTRACT]: {
    url: `https://velodrome.finance/deposit?token0=0x4200000000000000000000000000000000000042&token1=0x9560e827af36c94d2ac33a39bce1fe78631088db&stable=false`,
    displayName: "OP/VELO",
    shortDisplayName: "OP/VELO",
  },
  [VELODROMEV2_WETH_LUSD_CONTRACT]: {
    url: `https://velodrome.finance/deposit?token0=0x4200000000000000000000000000000000000006&token1=0xc40f949f8a4e094d1b49a23ea9241d289b7b2819&stable=false`,
    displayName: "WETH/LUSD",
    shortDisplayName: "WETH/LUSD",
  },
  [VELODROMEV2_SUSD_LUSD_CONTRACT]: {
    url: `https://velodrome.finance/deposit?token0=0x8c6f28f2f1a3c87f0f938b96d27520d9751ec8d9&token1=0xc40f949f8a4e094d1b49a23ea9241d289b7b2819&stable=true`,
    displayName: "sUSD/LUSD",
    shortDisplayName: "sUSD/LUSD",
  },
  [VELODROMEV2_ETH_GRAIN_CONTRACT]: {
    url: `https://velodrome.finance/deposit?token0=0x4200000000000000000000000000000000000006&token1=0xfd389dc9533717239856190f42475d3f263a270d&stable=false`,
    displayName: "ETH/GRAIN",
    shortDisplayName: "ETH/GRAIN",
  },
  [EQUALIZER_FTM_USDC_CONTRACT]: {
    url: `https://equalizer.exchange/liquidity/0x7547d05dFf1DA6B4A2eBB3f0833aFE3C62ABD9a1`,
    displayName: "FTM/USDC",
    shortDisplayName: "FTM/USDC",
  },
  [RETRO_GAMMA_WETH_USDC05_CONTRACT]: {
    url: `https://retro.finance/liquidity/managev3?currency0=0x2791bca1f2de4661ed88a30c99a7a9449aa84174&currency1=0x7ceb23fd6bc0add59e62ac25578270cff1b9f619`,
    displayName: "Gamma WETH/USDC 0.05",
    shortDisplayName: "GAMMA-WETH/USDC05",
  },
  [RETRO_GAMMA_WMATIC_USDC05_CONTRACT]: {
    url: `https://retro.finance/liquidity/managev3?currency0=0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270&currency1=0x2791bca1f2de4661ed88a30c99a7a9449aa84174`,
    displayName: "Gamma WMATIC/USDC 0.05",
    shortDisplayName: "GAMMA-WMATIC/USDC05",
  },
  [RETRO_ICHI_WETH_USDC05_CONTRACT]: {
    url: `https://retro.finance/liquidity/managev3?currency0=0x2791bca1f2de4661ed88a30c99a7a9449aa84174&currency1=0x7ceb23fd6bc0add59e62ac25578270cff1b9f619`,
    displayName: "Ichi WETH/USDC 0.05",
    shortDisplayName: "ICHI-WETH/USDC05",
  },
  [RETRO_ICHI_WMATIC_USDC05_CONTRACT]: {
    url: `https://retro.finance/liquidity/managev3?currency0=0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270&currency1=0x2791bca1f2de4661ed88a30c99a7a9449aa84174`,
    displayName: "Ichi WMATIC/USDC 0.05",
    shortDisplayName: "ICHI-WMATIC/USDC05",
  },
  [RETRO_GAMMA_USDC_CASH01_CONTRACT]: {
    url: `https://retro.finance/liquidity/managev3?currency0=0x2791bca1f2de4661ed88a30c99a7a9449aa84174&currency1=0x5d066d022ede10efa2717ed3d79f22f949f8c175`,
    displayName: "Gamma USDC/CASH 0.01",
    shortDisplayName: "GAMMA-USDC/CASH01",
  },
} as { [key: string]: PoolUIInfo };

type AmountInputProps = {
  buttonText: string;
  value?: string;
  setValue: (value: string) => any;
  max: string;
  onClick?: () => any;
};
const AmountInput = ({ buttonText, value, setValue, max, onClick }: AmountInputProps) => {
  const disabled = useMemo(() => value === undefined || value === "" || parseFloat(value) <= 0 || parseFloat(value) > parseFloat(max), [value, max]);
  return (
    <div className="flex flex-row w-full gap-2">
      <div className="relative w-4/6">
        <input type="number" min={0} max={max} value={value} placeholder="AMOUNT" className="flex w-full h-16 rounded-md text-3xl px-2 font-transformers text-black outline-none" onChange={(e) => setValue(e.target.value)} />
        <Button className="w-2/6 h-3/4 absolute top-2 right-2 bg-black px-2 text-3xl" onClick={() => setValue(max)}>
          MAX
        </Button>
      </div>
      <Button disabled={disabled} className="w-2/6" onClick={onClick}>
        {buttonText}
      </Button>
    </div>
  );
};

const chooseApproveOrOtherFromAllowance = (depositValue: string | undefined, depositText: string, setDepositText: Dispatch<SetStateAction<string>>, otherText: string, tokenInfo: TokenInfo) => {
  if (depositValue === undefined || depositValue == "") {
    depositText != otherText && setDepositText(otherText);
    return;
  }
  const amountToDeposit = ethers.utils.parseUnits(depositValue, tokenInfo.decimals);
  if (amountToDeposit.gt(tokenInfo.reliquaryAllowedBalance)) {
    depositText != "Approve" && setDepositText("Approve");
  } else {
    depositText != otherText && setDepositText(otherText);
  }
};

const displayTimer = (totalSeconds: number, short: boolean): string => {
  if (totalSeconds == 0) {
    return "-";
  }
  let remains = totalSeconds;
  const weeks = Math.floor(remains / 604800);
  remains = remains % 604800;
  const days = Math.floor(remains / 86400);
  remains = remains % 86400;

  const date = new Date(0);
  date.setSeconds(remains);

  const wDisplay = weeks > 0 ? weeks + (short ? "W " : weeks == 1 ? " week, " : " weeks, ") : "";
  const dDisplay = days > 0 ? days + (short ? "D " : days == 1 ? " day, " : " days, ") : "";
  return wDisplay + dDisplay + date.toISOString().substr(11, 8);
};

const displayTimeLong = (totalSeconds: number): string => {
  return displayTimer(totalSeconds, false);
};

const displayTimerShort = (totalSeconds: number): string => {
  return displayTimer(totalSeconds, true);
};

const Timer = ({ initialValue, increment, displayTimer }: { initialValue: number; increment: number; displayTimer: (totalSeconds: number) => string }) => {
  const [secondsToNextLevel, setSecondsToNextLevel] = useState(initialValue);

  useEffect(() => {
    const timer = setInterval(() => {
      setSecondsToNextLevel((value) => value + increment);
    }, 1000);
    return () => clearInterval(timer);
  }, [increment]);

  return <span>{displayTimer(Math.max(Math.round(secondsToNextLevel), 0))}</span>;
};

type RelicProps = {
  info: RelicInfo;
  reliquaryInfo: ReliquaryInfo;
  onClaim: () => any;
  onDeposit: (value: BigNumber) => any;
  onWithdraw: (value: BigNumber) => any;
  onLevelUp: () => any;
};

const Relic = ({ info, reliquaryInfo, onClaim, onDeposit, onWithdraw, onLevelUp }: RelicProps) => {
  const [depositText, setDepositText] = useState("Deposit");
  const [depositValue, setDepositValue] = useState<string | undefined>(undefined);
  const [withdrawValue, setWithdrawValue] = useState<string | undefined>(undefined);

  useEffect(() => {
    const { pools, tokens } = reliquaryInfo;
    const tokenInfo = tokens[pools[info.position.poolId].token];
    chooseApproveOrOtherFromAllowance(depositValue, depositText, setDepositText, "Deposit", tokenInfo);
  }, [depositText, depositValue, reliquaryInfo, info]);

  const { pools, tokens } = reliquaryInfo;
  const pool = pools[info.position.poolId];
  const poolUiInfo = poolTokenToUIInfo[pool.token];
  const tokenInfo = tokens[pool.token];
  const amountInRelic = ethers.utils.formatUnits(info.position.amount, tokenInfo.decimals);
  const levelInfo = pool.perLevelInfo[info.position.level];

  const maturity = useMemo(() => Date.now() * 1e-3 - info.position.entry, [info]);
  const secondsToNextLevel = useMemo(() => {
    const pool = reliquaryInfo.pools[info.position.poolId];
    return info.position.level < pool.levelCount - 1 ? pool.perLevelInfo[info.position.level + 1].requiredMaturity - maturity : 0;
  }, [info, reliquaryInfo, maturity]);

  return (
    <Block>
      <div className="flex md:flex-row sm:flex-col gap-2 w-full items-center">
        <div className="flex flex-col md:basis-72 flex-none">
          <p className="text-3xl font-transformers">Relic #{info.id}</p>
          <p>
            <span className="underline">Pool</span>: <ExternalLink href={poolUiInfo.url}>{poolUiInfo.displayName}</ExternalLink>
          </p>
          <p className="underline">Pending Rewards</p>
          <p>{ethers.utils.formatUnits(info.pendingReward, reliquaryInfo.rewardTokenDecimals)}</p>
          <p className="underline">Amount</p>
          <p>{amountInRelic}</p>
          <p className="underline">TVL</p>
          <p>${info.tvlUsd}</p>
          <p>
            <span className="underline">APR</span>: {levelInfo.apr.toFixed(2)}%
          </p>
          <p>
            <span className="underline">Level</span>: {info.position.level + 1} / {pool.levelCount}
          </p>
          <p>
            <span className="underline">Boost</span>: x{levelInfo.multiplier}
          </p>
          <p className="underline">Maturity</p>
          <p>
            <Timer initialValue={maturity} increment={1} displayTimer={displayTimeLong} />
          </p>
          {secondsToNextLevel > 0 ? (
            <>
              <p className="underline">Next level in</p>
              <p>
                <Timer initialValue={secondsToNextLevel} increment={-1} displayTimer={displayTimeLong} />
              </p>
            </>
          ) : (
            <></>
          )}
        </div>
        <div className="flex flex-row gap-2 grow">
          <div className="flex flex-col gap-4 grow">
            <div className="flex flex-row gap-2 flex-none">
              <Button disabled={info.pendingReward.isZero()} onClick={onClaim}>
                Claim
              </Button>
              <Button disabled={info.levelOnUpdate == info.position.level} onClick={onLevelUp}>
                Level up
              </Button>
              {/* TODO <Button>Transfer</Button> */}
            </div>
            <div className="flex flex-col gap-2 grow">
              <AmountInput
                max={ethers.utils.formatUnits(tokenInfo.userBalance, tokenInfo.decimals)}
                value={depositValue}
                setValue={setDepositValue}
                buttonText={depositText}
                onClick={() => onDeposit(ethers.utils.parseUnits(depositValue as string, tokenInfo.decimals))}
              />
              <AmountInput max={amountInRelic} value={withdrawValue} setValue={setWithdrawValue} buttonText="Withdraw" onClick={() => onWithdraw(ethers.utils.parseUnits(withdrawValue as string, tokenInfo.decimals))} />
            </div>
          </div>
        </div>
      </div>
    </Block>
  );
};

type RelicInfoProps = {
  info: RelicInfo;
  reliquaryInfo: ReliquaryInfo;
  onUpdate: () => any;
};

const RelicInfo = ({ info, reliquaryInfo, onUpdate }: RelicInfoProps) => {
  const { chain } = useContext(StateContext);
  const { pools, tokens } = reliquaryInfo;
  const pool = pools[info.position.poolId];
  const poolUiInfo = poolTokenToUIInfo[pool.token];
  const tokenInfo = tokens[pool.token];
  const amountInRelic = ethers.utils.formatUnits(info.position.amount, tokenInfo.decimals);
  const levelInfo = pool.perLevelInfo[info.position.level];

  const maturity = useMemo(() => Date.now() * 1e-3 - info.position.entry, [info]);
  return (
    <div className="flex flex-col md:basis-72 flex-none">
      <p className="text-3xl font-transformers">Relic #{info.id}</p>
      <Button className="h-9" onClick={onUpdate}>
        Update
      </Button>
      <p>
        <span className="underline">Owner</span>:{" "}
        <ExternalLink href={`${chain.blockExplorers.default.url}/address/${info.owner}`}>
          {info.owner.substring(0, 6)}...{info.owner.substring(info.owner.length - 2)}
        </ExternalLink>
      </p>
      <p>
        <span className="underline">Pool</span>: <ExternalLink href={poolUiInfo.url}>{poolUiInfo.displayName}</ExternalLink>
      </p>
      <p className="underline">Pending Rewards</p>
      <p>{ethers.utils.formatUnits(info.pendingReward, reliquaryInfo.rewardTokenDecimals)}</p>
      <p className="underline">Amount</p>
      <p>{amountInRelic}</p>
      <p className="underline">TVL</p>
      <p>{info.tvlUsd}</p>
      <p>
        <span className="underline">APR</span>: {levelInfo.apr.toFixed(2)}%
      </p>
      <p>
        <span className="underline">Level</span>: {info.position.level + 1} / {pool.levelCount}
      </p>
      <p>
        <span className="underline">Boost</span>: x{levelInfo.multiplier}
      </p>
      <p className="underline">Maturity</p>
      <p>
        <Timer initialValue={maturity} increment={1} displayTimer={displayTimerShort} />
      </p>
    </div>
  );
};

type PoolProps = {
  info: PoolInfo;
  reliquaryInfo: ReliquaryInfo;
};

const Pool = ({ info, reliquaryInfo }: PoolProps) => {
  const { tokens } = reliquaryInfo;
  const poolUiInfo = poolTokenToUIInfo[info.token];
  return (
    <div className="flex flex-col bg-gray-800 p-4">
      <p className="text-center bg-blue-800">{poolUiInfo.displayName}</p>
      <p>Reward Alloc: {(100 * info.allocShare).toFixed(2)} %</p>
      <p>
        TVL: ${info.tvlUsd} ({ethers.utils.formatEther(info.tvl)} {tokens[info.token].symbol})
      </p>
      <p>Average APR: {info.averageApr} %</p>
      {info.perLevelInfo.map((levelInfo, level) => {
        return (
          <p key={level}>
            <p className="text-center bg-gray-600">Level {level + 1}</p>
            <p> - Required maturity: {displayTimer(levelInfo.requiredMaturity, false)}</p>
            <p> - Multiplier x{levelInfo.multiplier}</p>
            <p> - Reward Alloc {(100 * levelInfo.allocShare).toFixed(2)} %</p>
            <p>
              {" "}
              - Reward rate ${levelInfo.rewardRateUsd} / second ({levelInfo.rewardRate} {reliquaryInfo.rewardTokenSymbol})
            </p>
            <p> - TVL ${levelInfo.tvlUsd}</p>
            <p> - APR: {levelInfo.apr > 0 ? levelInfo.apr.toFixed(2) : "-"} %</p>
          </p>
        );
      })}
    </div>
  );
};

type CreatePrimeRelicModalProps = {
  isOpen: boolean;
  onClose: () => any;
  reliquaryInfo: ReliquaryInfo;
  onCreate: (poolId: number, amount: BigNumber) => any;
  showInactivePools: boolean;
};

const CreatePrimeRelicModal = ({ isOpen, onClose, onCreate, reliquaryInfo, showInactivePools }: CreatePrimeRelicModalProps) => {
  const [selectedPoolIdx, setSelectedPoolIdx] = useState<number | undefined>(undefined);
  const [depositText, setDepositText] = useState("Create & Deposit");
  const [depositValue, setDepositValue] = useState<string | undefined>(undefined);

  useEffect(() => {
    if (selectedPoolIdx === undefined) {
      return;
    }
    const { pools, tokens } = reliquaryInfo;
    const tokenInfo = tokens[pools[selectedPoolIdx].token];
    chooseApproveOrOtherFromAllowance(depositValue, depositText, setDepositText, "Create & Deposit", tokenInfo);
  }, [depositText, depositValue, reliquaryInfo, selectedPoolIdx]);

  const { pools, tokens } = reliquaryInfo;
  const tokenInfo = selectedPoolIdx !== undefined ? tokens[pools[selectedPoolIdx].token] : undefined;

  const PoolSelector = ({ pool, selected, onSelect }: { pool: PoolInfo; selected: boolean; onSelect: () => any }) => {
    const poolUiInfo = poolTokenToUIInfo[pool.token];
    return (
      <div className={`${selected ? "bg-blue-600" : "bg-gray-800"} cursor-pointer p-2 w-full flex flex-col place-content-center`} onClick={onSelect}>
        <p className="text-center">{poolUiInfo.displayName}</p>
      </div>
    );
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose} className="min-w-[75%]">
      <p className="text-3xl font-transformers text-center">Create New Relic</p>
      <p>Choose a pool:</p>
      <div className="flex md:flex-row sm:flex-col gap-2">
        {reliquaryInfo.pools
          .map((poolInfo, poolIdx) => ({ ...poolInfo, poolIdx }))
          .filter((poolInfo) => poolInfo.token in poolTokenToUIInfo)
          .filter((poolInfo) => showInactivePools || poolInfo.allocPoint > 0)
          .map((poolInfo) => (
            <PoolSelector
              key={poolInfo.poolIdx}
              pool={poolInfo}
              selected={selectedPoolIdx == poolInfo.poolIdx}
              onSelect={() => {
                setDepositValue("");
                setSelectedPoolIdx(poolInfo.poolIdx);
              }}
            />
          ))}
      </div>
      {tokenInfo === undefined ? (
        <></>
      ) : (
        <>
          <ExternalLink href={poolTokenToUIInfo[tokenInfo.address].url}>
            <Button>Get Token</Button>
          </ExternalLink>
          <AmountInput
            max={ethers.utils.formatUnits(tokenInfo.userBalance, tokenInfo.decimals)}
            value={depositValue}
            setValue={setDepositValue}
            buttonText={depositText}
            onClick={() => onCreate(selectedPoolIdx as number, ethers.utils.parseUnits(depositValue as string, tokenInfo.decimals))}
          />
        </>
      )}
    </Modal>
  );
};

type AllRelicsProps = {
  reliquaryInfo: ReliquaryInfo;
  onUpdateRelic: (info: RelicInfo) => any;
};

function AllRelics({ reliquaryInfo, onUpdateRelic }: AllRelicsProps) {
  const [allRelics, setAllRelics] = useState<RelicInfo[] | undefined>(undefined);

  useEffect(() => {
    getAllRelics(reliquaryInfo)
      .then(setAllRelics)
      .catch((reason) => {
        Notification({
          type: "error",
          title: "getAllRelics: Unable to fetch onchain data",
          message: `${reason}`,
          link: "",
        });
      });
  }, [reliquaryInfo]);

  const totalPendingRewards = useMemo(() => {
    if (allRelics === undefined) {
      return 0;
    }
    let total = 0;
    for (const relic of allRelics) {
      total += parseFloat(ethers.utils.formatUnits(relic.pendingReward, reliquaryInfo.rewardTokenDecimals));
    }
    return total;
  }, [allRelics, reliquaryInfo]);

  return (
    <div>
      <p>
        Total pending rewards: {totalPendingRewards} / {ethers.utils.formatUnits(reliquaryInfo.rewardBalance, reliquaryInfo.rewardTokenDecimals)}
      </p>
      <div className="grid lg:grid-cols-4 gap-2">
        {allRelics !== undefined ? (
          allRelics.map((relicInfo) => <RelicInfo key={relicInfo.id} info={relicInfo} reliquaryInfo={reliquaryInfo} onUpdate={() => onUpdateRelic(relicInfo)} />)
        ) : (
          <span>
            <i className="fa fa-refresh fa-spin"></i> Loading
          </span>
        )}
      </div>
    </div>
  );
}

type ReliquaryProps = {
  title: string;
  displayAllLevelsPerPool: boolean;
  reliquaryInfo: ReliquaryInfo;
  reloadReliquaryInfo: () => {};
};

export default function Reliquary({ title, displayAllLevelsPerPool, reliquaryInfo, reloadReliquaryInfo }: ReliquaryProps) {
  const { chain } = useContext(StateContext);
  const [isReloadingReliquary, setIsReloadingReliquary] = useState(false);

  const onReload = useCallback(() => {
    console.log("Reloading reliquary");
    setIsReloadingReliquary(true);
    reloadReliquaryInfo();
  }, [reloadReliquaryInfo]);

  useEffect(() => {
    const timer = setInterval(() => {
      onReload();
    }, 60000);
    return () => clearInterval(timer);
  }, [onReload]);

  useEffect(() => {
    setIsReloadingReliquary(false);
  }, [reliquaryInfo]);

  const onClaimRelicRewards = useCallback(
    async (relicInfo: RelicInfo) => {
      await notificateTransaction(await claimRelicRewards(reliquaryInfo, relicInfo), "claim rewards");
      onReload();
    },
    [reliquaryInfo, onReload]
  );

  const onLevelUpRelic = useCallback(
    async (relicInfo: RelicInfo) => {
      await notificateTransaction(await updatePosition(reliquaryInfo, relicInfo), "level up relic");
      onReload();
    },
    [reliquaryInfo, onReload]
  );

  const onUpdateRelic = useCallback(
    async (relicInfo: RelicInfo) => {
      await notificateTransaction(await updatePosition(reliquaryInfo, relicInfo), "update relic");
      onReload();
    },
    [reliquaryInfo, onReload]
  );

  const approveIfRequired = useCallback(
    async (amount: BigNumber, tokenInfo: TokenInfo) => {
      if (!amount.gt(tokenInfo.reliquaryAllowedBalance)) {
        return false;
      }
      await notificateTransaction(
        await approve(reliquaryInfo, tokenInfo.address, reliquaryInfo.contract.address, amount, () => {
          onReload();
        }),
        `approve spending of ${ethers.utils.formatUnits(amount, tokenInfo.decimals)} ${tokenInfo.name} ${tokenInfo.symbol} by Reliquary Prime ${reliquaryInfo.contract.address}`
      );
      return true;
    },
    [reliquaryInfo, onReload]
  );

  const onRelicDeposit = useCallback(
    async (amount: BigNumber, relicInfo: RelicInfo) => {
      const tokenInfo = reliquaryInfo.tokens[reliquaryInfo.pools[relicInfo.position.poolId].token];
      if (!(await approveIfRequired(amount, tokenInfo))) {
        await notificateTransaction(await depositInRelic(reliquaryInfo, relicInfo, amount), `deposit ${ethers.utils.formatUnits(amount, tokenInfo.decimals)} ${tokenInfo.name} ${tokenInfo.symbol} in relic ${relicInfo.id}`);
      }
      onReload();
    },
    [reliquaryInfo, onReload, approveIfRequired]
  );

  const onRelicWithdraw = useCallback(
    async (amount: BigNumber, relicInfo: RelicInfo) => {
      const tokenInfo = reliquaryInfo.tokens[reliquaryInfo.pools[relicInfo.position.poolId].token];
      await notificateTransaction(await withdrawFromRelic(reliquaryInfo, relicInfo, amount), `withdraw ${ethers.utils.formatUnits(amount, tokenInfo.decimals)} ${tokenInfo.name} ${tokenInfo.symbol} from relic ${relicInfo.id}`);
      onReload();
    },
    [reliquaryInfo, onReload]
  );

  const onCreateRelic = useCallback(
    async (poolId: number, amount: BigNumber) => {
      const tokenInfo = reliquaryInfo.tokens[reliquaryInfo.pools[poolId].token];
      if (!(await approveIfRequired(amount, tokenInfo))) {
        await notificateTransaction(await createRelicAndDeposit(reliquaryInfo, poolId, amount), `create relic and deposit ${ethers.utils.formatUnits(amount, tokenInfo.decimals)} ${tokenInfo.name} ${tokenInfo.symbol}`);
      }
      onReload();
    },
    [reliquaryInfo, onReload, approveIfRequired]
  );

  const [createIsOpen, setCreateIsOpen] = useState(false);
  const closeCreateModal = () => {
    setCreateIsOpen(false);
  };
  const openCreateModal = () => {
    setCreateIsOpen(true);
  };

  const [showReliquaryDetails, setShowReliquaryDetails] = useState(false);
  const [showAllRelics, setShowAllRelics] = useState(false);
  const [showInactivePools, setShowInactivePools] = useState(false);

  return (
    <>
      <Block>
        <p className="text-3xl font-transformers">
          {title}{" "}
          <i
            className={`fa fa-refresh cursor-pointer ${isReloadingReliquary ? "fa-spin" : ""}`}
            onClick={() => {
              onReload();
            }}
          ></i>
        </p>
        <div className="flex flex-col gap-0 place-items-center">
          <p>
            Contract: <ExternalLink href={`${chain.blockExplorers.default.url}/address/${reliquaryInfo.contract.address}`}>{reliquaryInfo.contract.address}</ExternalLink>
          </p>
          <p>
            Reward token:{" "}
            <ExternalLink href={`${chain.blockExplorers.default.url}/token/${reliquaryInfo.rewardTokenAddr}`}>
              {reliquaryInfo.rewardTokenName} {reliquaryInfo.rewardTokenSymbol}
            </ExternalLink>
          </p>
          <p>TVL: ${reliquaryInfo.tvlUsd}</p>
          <p className="bg-blue-800 p-2">Average APR: {reliquaryInfo.averageApr.toFixed(2)} %</p>
          <p>Reward rate:</p>
          {/* <p>${reliquaryInfo.rewardRateUsd} / second ({reliquaryInfo.rewardRate} {reliquaryInfo.rewardTokenSymbol})</p> */}
          <p>
            ${(reliquaryInfo.rewardRateUsd * 3600 * 24 * 7).toFixed(2)} / week ({(reliquaryInfo.rewardRate * 3600 * 24 * 7).toFixed(3)} {reliquaryInfo.rewardTokenSymbol})
          </p>
          <p>
            <input type="checkbox" placeholder="Display inactive pools" title="display-inactive" checked={showInactivePools} onChange={() => setShowInactivePools(!showInactivePools)}></input>
            <label className="cursor-pointer" htmlFor="display-inactive" onClick={() => setShowInactivePools(!showInactivePools)}>
              {" "}
              Display inactive pools
            </label>
          </p>
        </div>
        <div className="flex md:flex-row sm:flex-col gap-2 w-full text-center">
          {reliquaryInfo.pools
            .map((poolInfo, poolIdx) => ({ ...poolInfo, poolIdx }))
            .filter((poolInfo) => poolInfo.token in poolTokenToUIInfo)
            .filter((poolInfo) => showInactivePools || poolInfo.allocPoint > 0)
            .map((poolInfo) => (
              <div key={poolInfo.poolIdx} className="bg-gray-800 p-2 w-full">
                <p className="bg-blue-800">{poolTokenToUIInfo[poolInfo.token].shortDisplayName}</p>
                <p>TVL: ${poolInfo.tvlUsd.toFixed(2)}</p>
                <p>APR: {poolInfo.averageApr.toFixed(2)} %</p>
                {displayAllLevelsPerPool ? (
                  <>
                    {poolInfo.perLevelInfo.map((levelInfo, level) => {
                      return (
                        <p key={level}>
                          <p className="bg-gray-600">Level {level + 1}</p>
                          <p>TVL: ${levelInfo.tvlUsd.toFixed(2)}</p>
                          <p>
                            APR: {levelInfo.apr > 0 ? levelInfo.apr.toFixed(2) : "-"}% {levelInfo.apr > 100 ? "🔥" : ""}
                          </p>
                        </p>
                      );
                    })}
                  </>
                ) : (
                  <></>
                )}
              </div>
            ))}
        </div>
        <Button
          onClick={() => {
            setShowReliquaryDetails((value) => !value);
          }}
        >
          Show pools details
        </Button>
        {showReliquaryDetails ? (
          <div className="flex flex-col w-full gap-2">
            {reliquaryInfo.pools
              .map((poolInfo, poolIdx) => ({ ...poolInfo, poolIdx }))
              .filter((poolInfo) => poolInfo.token in poolTokenToUIInfo)
              .filter((poolInfo) => showInactivePools || poolInfo.allocPoint > 0)
              .map((poolInfo) => (
                <Pool key={poolInfo.poolIdx} info={poolInfo} reliquaryInfo={reliquaryInfo} />
              ))}
          </div>
        ) : (
          <></>
        )}
        <Button
          onClick={() => {
            setShowAllRelics((value) => !value);
          }}
        >
          Show all relics
        </Button>
        {showAllRelics ? (
          <div className="flex flex-col w-full gap-2">
            <AllRelics reliquaryInfo={reliquaryInfo} onUpdateRelic={onUpdateRelic} />
          </div>
        ) : (
          <></>
        )}
      </Block>
      <Block>
        <p className="text-3xl font-transformers">Your prime relics</p>
        <div className="flex flex-col gap-1 place-items-center w-full">
          <p>Total Value: ${reliquaryInfo.totalUserDepositUsd.toFixed(2)}</p>
          <p>
            Total Pending Rewards: {ethers.utils.formatUnits(reliquaryInfo.totalUserPendingRewards, reliquaryInfo.rewardTokenDecimals)} {reliquaryInfo.rewardTokenSymbol} (${reliquaryInfo.totalUserPendingRewardsUsd.toFixed(2)})
          </p>
          <div className="flex md:flex-row sm:flex-col gap-2 w-full text-center">
            {reliquaryInfo.pools
              .map((poolInfo, poolIdx) => ({ ...poolInfo, poolIdx }))
              .filter((poolInfo) => poolInfo.token in poolTokenToUIInfo)
              .filter((poolInfo) => showInactivePools || poolInfo.allocPoint > 0)
              .map((poolInfo) => (
                <div key={poolInfo.poolIdx} className="bg-gray-800 p-2 w-full">
                  <p className="bg-blue-800">{poolTokenToUIInfo[poolInfo.token].shortDisplayName}</p>
                  <p>${reliquaryInfo.perPoolUserDepositsUsd[poolInfo.poolIdx].toFixed(2)}</p>
                </div>
              ))}
          </div>
        </div>
        <Button onClick={openCreateModal}>Create</Button>
      </Block>
      {reliquaryInfo.relics.map((relicInfo) => (
        <Relic
          key={relicInfo.id}
          info={relicInfo}
          reliquaryInfo={reliquaryInfo}
          onClaim={() => onClaimRelicRewards(relicInfo)}
          onDeposit={(value) => onRelicDeposit(value, relicInfo)}
          onWithdraw={(value) => onRelicWithdraw(value, relicInfo)}
          onLevelUp={() => onLevelUpRelic(relicInfo)}
        />
      ))}
      <CreatePrimeRelicModal isOpen={createIsOpen} onClose={closeCreateModal} onCreate={onCreateRelic} reliquaryInfo={reliquaryInfo} showInactivePools={showInactivePools} />
    </>
  );
}
