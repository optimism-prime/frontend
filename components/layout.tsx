import Link from 'next/link';
import Image from 'next/image';
import { ReactElement, useContext, useEffect, useMemo, useState } from 'react';
import Head from 'next/head';
import Nav from './nav/nav';
import NavMobile from './nav/navMobile';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ConnectButton } from '@rainbow-me/rainbowkit';
import Twitter from './svg/twitter';
import Discord from './svg/discord';
import Telegram from './svg/telegram';
import { ExternalLink } from './ui';
import Medium from './svg/medium';
import { StateContext } from '../context/StateContext';

interface layout {
  children: ReactElement;
}

export default function Layout({ children }: layout) {
  useEffect(() => {
    document.documentElement.lang = 'en-us';
    document.documentElement?.classList.add('scroll-smooth');
  }, []);
  const {
    backgroundIndex,
    setBackgroundIndex
  } = useContext(StateContext);
  const [backgroundStyle, setBackgroundStyle] = useState({})

  useEffect(() => {
    setBackgroundStyle({backgroundImage: `url(/banner/indexed/${('000' + backgroundIndex).slice(-3)}.jpg)`});
  }, [backgroundIndex])

  return (
    <div className={`flex flex-col justify-between bg-cover bg-fixed bg-no-repeat font-roboto-cond relative scroll-smooth min-h-screen`} style={backgroundStyle}>
      <Head>
        <title>OPTIMISM PRIME</title>
      </Head>
      {/* NAV DESKTOP */}
      <Nav />

      {/* LOGO MOBILE */}
      <div className="flex flex-col xl:hidden z-20 bg-gradient-to-b from-black">
        <Link href="/">
          <a>
            <div className="flex flex-row justify-center items-center font-transformers text-white text-3xl lg:text-5xl mt-2 mb-2">
              <Image
                src="/logo_round.png"
                alt="logo"
                width={96}
                height={96}
                unoptimized={true}
                quality={100}
              />
              <div className="ml-2">
                <span className="italic mr-[16px]">
                  OPTIMISM
                </span>
                <span>PRIME</span>
              </div>
            </div>
          </a>
        </Link>
        <div className="flex self-center pb-3">
          <ConnectButton accountStatus="address" label="CONNECT" />
        </div>
        <div className="flex xl:hidden flex-row justify-center items-center space-x-5 mb-8 bg-slate-900/40 rounded-full w-44 py-2 self-center">
          <ExternalLink
            href="https://twitter.com/Optimism_Pr"
            className="w-8 hover:opacity-70 scale-75 hover:scale-100"
          >
            <Twitter />
          </ExternalLink>
          <ExternalLink
            href="https://discord.gg/SpfdM2VwUy"
            className="w-8 hover:opacity-70 scale-75 hover:scale-100"
          >
            <Discord />
          </ExternalLink>
          <ExternalLink
            href="https://medium.com/optimism-prime"
            className="w-8 hover:opacity-70 scale-75 hover:scale-100"
          >
            <Medium />
          </ExternalLink>
        </div>
      </div>
      <ToastContainer
        containerId="toast-notification"
        position="top-right"
        autoClose={6000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick={false}
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />

      <main className="flex items-center flex-col md:px-12 font-roboto-cond justify-between h-full">
        <>{children}</>
      </main>

      {/* FOOTER */}
      <footer
        className={`
          flex flex-col
          h-10
          pb-[79px] xl:pb-0
          lg:justify-end
          mt-16
        `}
      >
        <div className="flex justify-center bg-black h-10 text-red-900">
            <span className="flex items-center">Background:</span>
            <input type="number" className="bg-black ml-2" min="0" max="44" value={backgroundIndex} onChange={(e) => setBackgroundIndex(parseInt(e.target.value))} />
        </div>
        {/* NAV MOBILE */}
        <NavMobile />
      </footer>
    </div>
  );
}
