import Link from 'next/link';
import { useContext } from 'react';
import { nav } from '../../utils/constants';
import { Page } from '../../utils/enums';
import { StateContext } from '../../context/StateContext';

export default function Navbar({
  styleFirstLink,
  styleFirstLinkTwo,
  styleSecondLink,
}: {
  styleFirstLink: string;
  styleFirstLinkTwo: string;
  styleSecondLink: string;
}) {
  const { page, readyToMint, setPage } = useContext(StateContext);

  return (
    <>
      {nav
        .filter((item) =>
          item.type === Page.nft ? (readyToMint ? true : false) : true
        )
        .map((item, index) => (
          <Link key={index} href={item.href}>
            <a
              onClick={() => setPage(item.type)}
              className={`${styleFirstLink} ${
                page === item.type && { styleFirstLinkTwo }
              }`}
            >
              {item.title}
            </a>
          </Link>
        ))}
        <a href="https://velodrome.finance/swap?from=eth&to=0x676f784d19c7f1ac6c6beaeaac78b02a73427852" rel="noreferrer" target="_blank" className={styleSecondLink}>
          BUY $OPP
        </a>
    </>
  );
}
