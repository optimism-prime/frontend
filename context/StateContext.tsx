import { useRouter } from 'next/router';
import { useState, createContext, useEffect, ReactNode } from 'react';
import { useNetwork, useSigner } from 'wagmi';
import type { Chain } from '@rainbow-me/rainbowkit';
import { ethers } from 'ethers';
import { Page } from '../utils/enums';
import { nav, STAKE_SEASONS } from '../utils/constants';
import { isBefore, differenceInSeconds, fromUnixTime } from 'date-fns';
import { useLocalStorage } from '../utils/localStorage';

export const StateContext = createContext({
  page: Page.dashboard,
  setPage: (value: Page) => {},

  isLoading: false,
  setIsLoading: (value: boolean) => {},

  chain: useNetwork.prototype,
  setChain: (
    value:
      | (Chain & {
          unsupported?: boolean | undefined;
        })
      | null
  ) => {},

  signer: null as ethers.Signer | null,
  setSigner: (value: ethers.Signer | null) => {},

  currentSeason: 0,
  setCurrentSeason: (value: number) => {},

  balanceOpp: 0,
  setBalanceOpp: (value: number) => {},
  totalStaked: 0,
  setTotalStaked: (value: number) => {},
  userStakeBalance: 0,
  setUserStakeBalance: (value: number) => {},
  rewards: 0,
  setRewards: (value: number) => {},
  apr: 0,
  setApr: (value: number) => {},
  lockedUntil: 0,
  setLockedUntil: (value: number) => {},
  isReloading: false,
  setIsReloading: (value: boolean) => {},
  max: 0,
  setMax: (value: number) => {},
  readyToMint: false,
  setReadyToMint: (value: boolean) => {},
  countdown: '',
  setCountdown: (value: string) => {},

  backgroundIndex: 0,
  setBackgroundIndex: (value: number) => {},
});

type Props = {
  children?: ReactNode;
};

export const StateProvider = ({ children }: Props) => {
  const router = useRouter();
  const [page, setPage] = useState(Page.dashboard);
  const [isLoading, setIsLoading] = useState(false);
  const [chain, setChain] = useState<
    | (Chain & {
        unsupported?: boolean | undefined;
      })
    | null
  >(null);
  const [signer, setSigner] = useState<any>(null);
  const [currentSeason, setCurrentSeason] = useState(STAKE_SEASONS.length);
  const [balanceOpp, setBalanceOpp] = useState(0);
  const [totalStaked, setTotalStaked] = useState(0);
  const [userStakeBalance, setUserStakeBalance] = useState(0);
  const [rewards, setRewards] = useState(0);
  const [apr, setApr] = useState(0);
  const [lockedUntil, setLockedUntil] = useState(0);
  const [isReloading, setIsReloading] = useState(false);
  const [max, setMax] = useState(0);
  const [readyToMint, setReadyToMint] = useState(false);
  const [countdown, setCountdown] = useState<string>('0 DAYS, 00:00:00');
  const [backgroundIndex, setBackgroundIndex] = useLocalStorage("opp_backgroundIndex", 33);

  const { chain: activeChain } = useNetwork();
  const { data: walletSigner } = useSigner();

  useEffect(() => {
    setChain(
      activeChain
        ? (activeChain as Chain & {
            unsupported?: boolean | undefined;
          })
        : null
    );
  }, [activeChain]);

  useEffect(() => {
    if (walletSigner && walletSigner.provider?.call) {
      return setSigner(walletSigner as ethers.Signer);
    }

    setSigner(null);
  }, [walletSigner]);

  useEffect(() => {
    if (router.isReady) {
      const page = nav.find((item) => item.href === router.asPath);
      setPage(page?.type as Page);
    }
  }, [router]);

  useEffect(() => {
    const countdownInterval = setInterval(() => {
      let target = fromUnixTime(1665514800);
      if (isBefore(target, new Date())) {
        setReadyToMint(true);
        clearInterval(countdownInterval);
      }

      const diff = differenceInSeconds(target, new Date());
      const days = Math.floor(diff / 86400);
      const hours = Math.floor((diff - days * 86400) / 3600);
      const minutes = Math.floor((diff - days * 86400 - hours * 3600) / 60);
      const seconds = diff - days * 86400 - hours * 3600 - minutes * 60;

      setCountdown(`${days} days, ${hours}:${minutes}:${seconds}`);
    }, 1000);
  }, []);

  return (
    <StateContext.Provider
      value={{
        page,
        setPage,
        isLoading,
        setIsLoading,
        chain,
        setChain,
        signer,
        setSigner,
        currentSeason,
        setCurrentSeason,
        apr,
        setApr,
        balanceOpp,
        setBalanceOpp,
        totalStaked,
        setTotalStaked,
        userStakeBalance,
        setUserStakeBalance,
        rewards,
        setRewards,
        lockedUntil,
        setLockedUntil,
        isReloading,
        setIsReloading,
        max,
        setMax,
        readyToMint,
        setReadyToMint,
        countdown,
        setCountdown,
        backgroundIndex,
        setBackgroundIndex,
      }}
    >
      {children}
    </StateContext.Provider>
  );
};

export default StateProvider;
