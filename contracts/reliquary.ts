import { BigNumber, Contract, ethers } from "ethers";
import {
  RELIQUARY_PRIME_ABI,
  TOKEN_ABI,
  RELIQUARY_EMISSION_CURVE_ABI,
  OPP_CONTRACT,
  VELODROME_OPP_ETH_CONTRACT_V1,
  VELODROME_OPP_FBOMB_CONTRACT_V1,
  VELODROME_OPP_OP_CONTRACT_V1,
  VELODROME_OPP_FBOMB_CONTRACT_V2,
  VELODROME_OPP_ETH_CONTRACT_V2,
  VELODROME_OPP_OP_CONTRACT_V2,
  VELODROME_OPP_OPXVELO_CONTRACT_V2,
  OPP_BORROWABLE_CONTRACT,
  VELODROME_OP_USDC_CONTRACT,
  OP_CONTRACT,
  FANTOM_FTM_ADDR,
  OPTIMISM_WETH_ADDR,
  VELODROMEV1_ETH_USDC_CONTRACT,
  EQUALIZER_FTM_USDC_CONTRACT,
  VELODROMEV2_OP_VELO_CONTRACT,
  VELODROMEV2_WETH_LUSD_CONTRACT,
  VELODROMEV2_SUSD_LUSD_CONTRACT,
  VELODROMEV1_OP_VELO_CONTRACT,
  VELODROMEV1_WETH_LUSD_CONTRACT,
  VELODROMEV1_SUSD_LUSD_CONTRACT,
  POLYGON_WMATIC_ADDR,
  RETRO_ICHI_WMATIC_USDC05_CONTRACT,
  RETRO_GAMMA_WETH_USDC05_CONTRACT,
  RETRO_ICHI_WETH_USDC05_CONTRACT,
  RETRO_GAMMA_USDC_CASH01_CONTRACT,
  UNISWAP_POLYGON_MATIC_USDC_CONTRACT,
  RETRO_GAMMA_WMATIC_USDC05_CONTRACT,
  ACTIVE_LIQUIDITY_MANAGER_ABI,
  VELODROMEV2_ETH_GRAIN_CONTRACT,
  UNIV2_PAIR_ABI,
  VELODROME_OPP_2192_CONTRACT_V2,
  BEETHOVENX_OPTIMISM_PRIME_SOUNDWAVE,
  BEETHOVENX_OPTIMISM_VAULT,
  BALANCER_VAULT_ABI,
} from "../utils/constants";
import { getPairInformationByChain, getPairsMatchingBaseTokenAddress } from "dexscreener-api";

export type PositionInfoStruct = {
  amount: BigNumber;
  rewardDebt: BigNumber;
  rewardCredit: BigNumber;
  entry: number;
  poolId: number;
  level: number;
};

export type RelicInfo = {
  id: number;
  position: PositionInfoStruct;
  levelOnUpdate: number;
  pendingReward: BigNumber;
  owner: string;
  tvlUsd: number;
  pendingRewardUsd: number;
};

export type LevelInfoStruct = {
  requiredMaturities: number[];
  multipliers: number[];
  balance: BigNumber[];
  tvlUsd: number[];
};

export type PerLevelInfoStruct = {
  requiredMaturity: number;
  multiplier: number;
  balance: BigNumber;
  tvlUsd: number;
  allocShare: number;
  rewardRate: number;
  rewardRateUsd: number;
  apr: number;
};

export type TokenInfo = {
  userBalance: BigNumber;
  reliquaryAllowedBalance: BigNumber;
  address: string;
  name: string;
  symbol: string;
  decimals: number;
  priceUsd: number;
};

export type PoolInfo = {
  accRewardPerShare: BigNumber;
  lastRewardTime: number;
  allocPoint: number;
  allocShare: number;
  name: string;
  allowPartialWithdrawals: boolean;
  token: string;
  levelCount: number;
  levelInfo: LevelInfoStruct;
  perLevelInfo: PerLevelInfoStruct[];
  tvl: BigNumber;
  tvlUsd: number;
  averageApr: number;
};

export type ReliquaryInfo = {
  contract: Contract;
  tvlUsd: number;
  averageApr: number;
  rewardTokenAddr: string;
  rewardTokenName: string;
  rewardTokenSymbol: string;
  rewardTokenDecimals: number;
  rewardTokenPrice: number;
  rewardBalance: BigNumber;
  rewardRate: number;
  rewardRateUsd: number;
  secondsToDistribute: number;
  totalAllocPoint: number;
  relics: RelicInfo[];
  pools: PoolInfo[];
  tokens: { [key: string]: TokenInfo };
  perPoolUserDepositsUsd: number[];
  totalUserDepositUsd: number;
  totalUserPendingRewardsUsd: number;
  totalUserPendingRewards: BigNumber;
};

const getReliquaryContract = (signer: ethers.Signer, address: string) => {
  return new ethers.Contract(address, RELIQUARY_PRIME_ABI, signer);
};

async function getRelicInfo(relicId: number, reliquaryContract: Contract): Promise<RelicInfo> {
  const position = await reliquaryContract.getPositionForId(relicId);
  const relicPosition = {
    amount: position[0],
    rewardDebt: position[1],
    rewardCredit: position[2],
    entry: position[3].toNumber(),
    poolId: position[4].toNumber(),
    level: position[5].toNumber(),
  };
  const pendingReward = await reliquaryContract.pendingReward(relicId);
  const levelOnUpdate = await reliquaryContract.levelOnUpdate(relicId);
  return {
    id: relicId,
    position: relicPosition,
    pendingReward,
    levelOnUpdate: levelOnUpdate.toNumber(),
    owner: await reliquaryContract.ownerOf(relicId),
    tvlUsd: 0,
    pendingRewardUsd: 0,
  };
}

export async function getReliquaryInfo(signer: ethers.Signer, reliquaryAddress: string): Promise<ReliquaryInfo> {
  const contract = getReliquaryContract(signer, reliquaryAddress);
  const userAddress = await signer.getAddress();
  const relicBalance = (await contract.balanceOf(userAddress)).toNumber();
  const relics = new Array(relicBalance);
  await Promise.all(
    Array.from(relics.keys()).map(async (tokenIdx) => {
      const relicId = await contract.tokenOfOwnerByIndex(userAddress, tokenIdx);
      relics[tokenIdx] = await getRelicInfo(relicId.toNumber(), contract);
    })
  );
  const totalAllocPoint = (await contract.totalAllocPoint()).toNumber();
  const poolLength = (await contract.poolLength()).toNumber();
  const pools = new Array<PoolInfo>(poolLength);
  const tokenSet = new Set<string>();
  const perPoolUserDepositsUsd = new Array<number>(poolLength);
  await Promise.all(
    Array.from(pools.keys()).map(async (poolIdx) => {
      const poolInfo = await contract.getPoolInfo(poolIdx);
      const token = await contract.poolToken(poolIdx);
      tokenSet.add(token);
      const levelInfo = await contract.getLevelInfo(poolIdx);
      const allocPoint = poolInfo[2].toNumber();
      pools[poolIdx] = {
        accRewardPerShare: poolInfo[0],
        lastRewardTime: poolInfo[1].toNumber(),
        allocPoint,
        allocShare: allocPoint / totalAllocPoint,
        name: poolInfo[3],
        allowPartialWithdrawals: poolInfo[4],
        token: token,
        levelCount: levelInfo[0].length,
        levelInfo: {
          requiredMaturities: levelInfo[0].map((x: BigNumber) => x.toNumber()),
          multipliers: levelInfo[1].map((x: BigNumber) => x.toNumber()),
          balance: levelInfo[2],
          tvlUsd: [], // Computed later
        },
        perLevelInfo: [], // Computed later
        tvl: ethers.utils.parseEther("0"), // Computed later
        tvlUsd: 0, // Computed later
        averageApr: 0, // Computed later
      };
      perPoolUserDepositsUsd[poolIdx] = 0;
    })
  );

  const rewardTokenAddr = await contract.rewardToken();
  const rewardTokenContract = new ethers.Contract(rewardTokenAddr, TOKEN_ABI, signer);

  const rewardTokenPrice = await (async () => {
    let pairInfo = undefined;
    switch (rewardTokenAddr) {
      case OP_CONTRACT:
        pairInfo = await getPairInformationByChain("optimism", VELODROME_OP_USDC_CONTRACT);
        break;
      case OPTIMISM_WETH_ADDR:
        pairInfo = await getPairInformationByChain("optimism", VELODROMEV1_ETH_USDC_CONTRACT);
        break;
      case FANTOM_FTM_ADDR:
        pairInfo = await getPairInformationByChain("fantom", EQUALIZER_FTM_USDC_CONTRACT);
        break;
      case POLYGON_WMATIC_ADDR:
        pairInfo = await getPairInformationByChain("polygon", UNISWAP_POLYGON_MATIC_USDC_CONTRACT);
        break;
    }
    return pairInfo !== undefined ? parseFloat(pairInfo.pair.priceUsd || "0") : 0;
  })();

  const oppPriceUsd = rewardTokenAddr === OP_CONTRACT ? await getOppPriceUsd(signer) : 0; // ugly check that we are processing the Reliquary Prime

  const tokens = {} as { [key: string]: TokenInfo };
  await Promise.all(
    Array.from(tokenSet).map(async (tokenAddr) => {
      const tokenContract = new ethers.Contract(tokenAddr, TOKEN_ABI, signer);
      const decimals = await tokenContract.decimals();

      const priceUsd = await getTokenPrice(tokenContract, oppPriceUsd, rewardTokenPrice);

      tokens[tokenAddr] = {
        userBalance: await tokenContract.balanceOf(userAddress),
        reliquaryAllowedBalance: await tokenContract.allowance(userAddress, reliquaryAddress),
        address: tokenAddr,
        name: await tokenContract.name(),
        symbol: await tokenContract.symbol(),
        decimals,
        priceUsd,
      };
    })
  );

  const emissionCurveAddr = await contract.emissionCurve();
  const emissionCurve = new ethers.Contract(emissionCurveAddr, RELIQUARY_EMISSION_CURVE_ABI, signer);

  const rewardTokenDecimals = await rewardTokenContract.decimals();
  const rewardRate = parseFloat(ethers.utils.formatUnits(await emissionCurve.getRate(0), rewardTokenDecimals));

  const reliquaryRewardRateUsd = rewardRate * rewardTokenPrice;

  let totalTvlUsd = 0;
  let averageApr = 0;
  pools.forEach((pool) => {
    const tokenInfo = tokens[pool.token];
    const priceUsd = tokenInfo.priceUsd;
    let tvlUsd = 0;
    pool.levelInfo.tvlUsd = pool.levelInfo.balance.map((balance) => {
      const poolTvl = parseFloat(ethers.utils.formatUnits(balance, tokenInfo.decimals)) * priceUsd;
      tvlUsd += poolTvl;
      pool.tvl = pool.tvl.add(balance);
      return poolTvl;
    });
    pool.tvlUsd = tvlUsd;
    pool.averageApr = 0;
    totalTvlUsd += tvlUsd;
    let totalMultiplier = 0;
    pool.perLevelInfo = Array.from(new Array(pool.levelCount).keys()).map((levelIdx) => {
      const multiplier = pool.levelInfo.multipliers[levelIdx];
      const tvlUsd = pool.levelInfo.tvlUsd[levelIdx];
      totalMultiplier += tvlUsd * multiplier;
      return {
        requiredMaturity: pool.levelInfo.requiredMaturities[levelIdx],
        multiplier: multiplier,
        balance: pool.levelInfo.balance[levelIdx],
        tvlUsd,
        allocShare: 0,
        apr: 0,
        rewardRateUsd: 0,
        rewardRate: 0,
      };
    });
    pool.perLevelInfo.forEach((levelInfo) => {
      levelInfo.allocShare = totalMultiplier > 0 ? ((levelInfo.tvlUsd * levelInfo.multiplier) / totalMultiplier) * pool.allocShare : 0;
      levelInfo.rewardRate = levelInfo.allocShare * rewardRate;
      levelInfo.rewardRateUsd = levelInfo.allocShare * reliquaryRewardRateUsd;
      levelInfo.apr = levelInfo.tvlUsd > 0 ? (100 * levelInfo.rewardRateUsd * 3600 * 24 * 365) / levelInfo.tvlUsd : 0;
      pool.averageApr += levelInfo.apr * levelInfo.tvlUsd;
      averageApr += levelInfo.apr * levelInfo.tvlUsd;
    });
    pool.averageApr = pool.tvlUsd > 0 ? pool.averageApr / pool.tvlUsd : 0;
  });
  averageApr = totalTvlUsd > 0 ? averageApr / totalTvlUsd : 0;

  const rewardBalance = await rewardTokenContract.balanceOf(reliquaryAddress);
  const secondsToDistribute = parseFloat(ethers.utils.formatUnits(rewardBalance, rewardTokenDecimals)) / rewardRate;

  const reliquaryInfo = {
    contract,
    tvlUsd: totalTvlUsd,
    averageApr,
    rewardTokenAddr,
    rewardTokenName: await rewardTokenContract.name(),
    rewardTokenDecimals: rewardTokenDecimals,
    rewardTokenSymbol: await rewardTokenContract.symbol(),
    rewardTokenPrice,
    rewardBalance,
    rewardRate,
    rewardRateUsd: reliquaryRewardRateUsd,
    secondsToDistribute,
    totalAllocPoint,
    relics,
    pools,
    tokens,
    perPoolUserDepositsUsd,
    totalUserDepositUsd: 0,
    totalUserPendingRewardsUsd: 0,
    totalUserPendingRewards: ethers.utils.parseEther("0"),
  } as ReliquaryInfo;

  const [totalUserDepositUsd, totalUserPendingRewardsUsd, totalUserPendingRewards] = computeRelicsTvlAndRewards(relics, reliquaryInfo);
  reliquaryInfo.totalUserDepositUsd = totalUserDepositUsd;
  reliquaryInfo.totalUserPendingRewardsUsd = totalUserPendingRewardsUsd;
  reliquaryInfo.totalUserPendingRewards = totalUserPendingRewards;

  for (const relicInfo of relics) {
    perPoolUserDepositsUsd[relicInfo.position.poolId] += relicInfo.tvlUsd;
  }
  return reliquaryInfo;
}

function computeRelicsTvlAndRewards(relics: RelicInfo[], reliquaryInfo: ReliquaryInfo): [number, number, BigNumber] {
  const { pools, tokens } = reliquaryInfo;
  let totalUserDepositUsd = 0;
  let totalPendingRewardsUsd = 0;
  let totalPendingRewards = ethers.utils.parseEther("0");
  for (const relicInfo of relics) {
    const pool = pools[relicInfo.position.poolId];
    const tokenInfo = tokens[pool.token];
    const amountInRelic = ethers.utils.formatUnits(relicInfo.position.amount, tokenInfo.decimals);
    relicInfo.tvlUsd = parseFloat(amountInRelic) * tokenInfo.priceUsd;

    const pendingReward = ethers.utils.formatUnits(relicInfo.pendingReward, reliquaryInfo.rewardTokenDecimals);
    relicInfo.pendingRewardUsd = parseFloat(pendingReward) * reliquaryInfo.rewardTokenPrice;

    totalUserDepositUsd += relicInfo.tvlUsd;
    totalPendingRewardsUsd += relicInfo.pendingRewardUsd;
    totalPendingRewards = totalPendingRewards.add(relicInfo.pendingReward);
  }
  return [totalUserDepositUsd, totalPendingRewardsUsd, totalPendingRewards];
}

async function getTokenPrice(tokenContract: Contract, oppPriceUsd: number, rewardTokenPrice: number) {
  if (tokenContract.address == VELODROME_OPP_ETH_CONTRACT_V2) {
    tokenContract = new ethers.Contract(VELODROME_OPP_ETH_CONTRACT_V2, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROME_OPP_FBOMB_CONTRACT_V2) {
    tokenContract = new ethers.Contract(VELODROME_OPP_FBOMB_CONTRACT_V1, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROME_OPP_OP_CONTRACT_V2) {
    tokenContract = new ethers.Contract(VELODROME_OPP_OP_CONTRACT_V2, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROME_OPP_OPXVELO_CONTRACT_V2) {
    tokenContract = new ethers.Contract(VELODROME_OPP_OPXVELO_CONTRACT_V2, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROME_OPP_2192_CONTRACT_V2) {
    tokenContract = new ethers.Contract(VELODROME_OPP_2192_CONTRACT_V2, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROMEV2_OP_VELO_CONTRACT) {
    tokenContract = new ethers.Contract(VELODROMEV2_OP_VELO_CONTRACT, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROMEV2_WETH_LUSD_CONTRACT) {
    tokenContract = new ethers.Contract(VELODROMEV2_WETH_LUSD_CONTRACT, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROMEV2_SUSD_LUSD_CONTRACT) {
    tokenContract = new ethers.Contract(VELODROMEV2_SUSD_LUSD_CONTRACT, TOKEN_ABI, tokenContract.signer);
  } else if (tokenContract.address == VELODROMEV2_ETH_GRAIN_CONTRACT) {
    tokenContract = new ethers.Contract(VELODROMEV2_ETH_GRAIN_CONTRACT, UNIV2_PAIR_ABI, tokenContract.signer);
    const reserves = await tokenContract.getReserves();
    const totalSupply = await tokenContract.totalSupply();
    const ethLiquidity = reserves[0].mul(2);
    const usdLiquidity = rewardTokenPrice * parseFloat(ethers.utils.formatEther(ethLiquidity));
    return usdLiquidity / parseFloat(ethers.utils.formatEther(totalSupply));
  } else if (tokenContract.address == BEETHOVENX_OPTIMISM_PRIME_SOUNDWAVE) {
    tokenContract = new ethers.Contract(BEETHOVENX_OPTIMISM_PRIME_SOUNDWAVE, TOKEN_ABI, tokenContract.signer);
  }

  if (
    [
      RETRO_ICHI_WETH_USDC05_CONTRACT.toLowerCase(),
      RETRO_ICHI_WMATIC_USDC05_CONTRACT.toLowerCase(),
      RETRO_GAMMA_WETH_USDC05_CONTRACT.toLowerCase(),
      RETRO_GAMMA_WMATIC_USDC05_CONTRACT.toLowerCase(),
      RETRO_GAMMA_USDC_CASH01_CONTRACT.toLowerCase(),
    ].includes(tokenContract.address.toLowerCase())
  ) {
    const almContract = new ethers.Contract(tokenContract.address, ACTIVE_LIQUIDITY_MANAGER_ABI, tokenContract.signer);
    const token0 = new ethers.Contract(await almContract.token0(), TOKEN_ABI, tokenContract.signer);
    const token1 = new ethers.Contract(await almContract.token1(), TOKEN_ABI, tokenContract.signer);
    const tokensResponse = await getPairsMatchingBaseTokenAddress(`${token0.address},${token1.address}`);
    let price0 = undefined;
    let price1 = undefined;
    for (const pair of tokensResponse.pairs) {
      if (pair.baseToken.address == token0.address && pair.priceUsd !== undefined && price0 === undefined) {
        price0 = parseFloat(pair.priceUsd);
      }
      if (pair.baseToken.address == token1.address && pair.priceUsd !== undefined && price1 === undefined) {
        price1 = parseFloat(pair.priceUsd);
      }
      if (price0 !== undefined && price1 !== undefined) {
        break;
      }
    }
    if (price0 === undefined || price1 === undefined) {
      return 0;
    }
    const [amount0, amount1] = await almContract.getTotalAmounts();
    const totalValue0 = price0 * parseFloat(ethers.utils.formatUnits(amount0, await token0.decimals()));
    const totalValue1 = price1 * parseFloat(ethers.utils.formatUnits(amount1, await token1.decimals()));

    const totalSupply = await tokenContract.totalSupply();
    return (totalValue0 + totalValue1) / parseFloat(ethers.utils.formatEther(totalSupply));
  }

  const tokenAddr = tokenContract.address;
  if (tokenAddr == OPP_CONTRACT) {
    return oppPriceUsd;
  }
  if (tokenAddr == OPP_BORROWABLE_CONTRACT) {
    const oppBorrowableContract = await new ethers.Contract(
      OPP_BORROWABLE_CONTRACT,
      [
        {
          inputs: [],
          name: "exchangeRate",
          outputs: [
            {
              internalType: "uint256",
              name: "",
              type: "uint256",
            },
          ],
          stateMutability: "view",
          type: "function",
        },
      ],
      tokenContract.signer
    );
    const exchangeRate = await oppBorrowableContract.exchangeRate();
    return oppPriceUsd * parseFloat(ethers.utils.formatEther(exchangeRate));
  }

  const oppContract = new ethers.Contract(OPP_CONTRACT, TOKEN_ABI, tokenContract.signer);

  const totalSupply = await tokenContract.totalSupply();
  if (tokenAddr == EQUALIZER_FTM_USDC_CONTRACT) {
    const pairsResponse = await getPairInformationByChain("fantom", tokenAddr);
    const liquidity = pairsResponse.pair?.liquidity?.usd || 0;
    return liquidity / parseFloat(ethers.utils.formatEther(totalSupply));
  }
  if (tokenAddr == BEETHOVENX_OPTIMISM_PRIME_SOUNDWAVE) {
    const vaultContract = new ethers.Contract(BEETHOVENX_OPTIMISM_VAULT, BALANCER_VAULT_ABI, tokenContract.signer);
    const tokenInfo = await vaultContract.getPoolTokenInfo("0xadf86a03af1c77d81380f9fa7c4c797a3ebf2d3a000100000000000000000146", OPP_CONTRACT);
    const oppBalance = parseFloat(ethers.utils.formatEther(tokenInfo[0]));
    const liquidity = (oppPriceUsd * oppBalance) / 0.3; // UniV2 LP
    return liquidity / parseFloat(ethers.utils.formatEther(totalSupply));
  }
  const oppBalance = parseFloat(ethers.utils.formatEther(await oppContract.balanceOf(tokenAddr)));
  if (oppBalance > 0) {
    const liquidity = 2 * oppPriceUsd * oppBalance; // UniV2 LP
    return liquidity / parseFloat(ethers.utils.formatEther(totalSupply));
  }
  try {
    const pairsResponse = await getPairInformationByChain("optimism", tokenAddr);
    const liquidity = pairsResponse.pair?.liquidity?.usd || 0;
    return liquidity / parseFloat(ethers.utils.formatEther(totalSupply));
  } catch (e) {
    return 0;
  }
}

export async function getAllRelics(reliquaryInfo: ReliquaryInfo): Promise<RelicInfo[]> {
  const relicTotalSupply = (await reliquaryInfo.contract.totalSupply()).toNumber();
  const relics = new Array(relicTotalSupply);
  await Promise.all(
    Array.from(relics.keys()).map(async (tokenIdx) => {
      const relicId = await reliquaryInfo.contract.tokenByIndex(tokenIdx);
      relics[tokenIdx] = await getRelicInfo(relicId.toNumber(), reliquaryInfo.contract);
    })
  );
  computeRelicsTvlAndRewards(relics, reliquaryInfo);
  return relics.sort((a, b) => a.id - b.id);
}

export async function claimRelicRewards(reliquaryInfo: ReliquaryInfo, relicInfo: RelicInfo) {
  const userAddress = await reliquaryInfo.contract.signer.getAddress();
  return await reliquaryInfo.contract.harvest(relicInfo.id, userAddress);
}

export async function updatePosition(reliquaryInfo: ReliquaryInfo, relicInfo: RelicInfo) {
  return await reliquaryInfo.contract.updatePosition(relicInfo.id);
}

export async function approve(reliquaryInfo: ReliquaryInfo, tokenAddr: string, spenderAddr: string, amount: BigNumber, onApproval?: () => any) {
  const contract = new ethers.Contract(tokenAddr, TOKEN_ABI, reliquaryInfo.contract.signer);
  if (onApproval) {
    contract.once(contract.filters.Approval(await reliquaryInfo.contract.signer.getAddress(), spenderAddr), (owner: string, spender: string, value: BigNumber) => {
      console.log(`Approval from ${owner} to ${spender} of value ${value}`);
      onApproval();
    });
  }
  return await contract.approve(spenderAddr, amount);
}

export async function depositInRelic(reliquaryInfo: ReliquaryInfo, relicInfo: RelicInfo, amount: BigNumber) {
  return await reliquaryInfo.contract.deposit(amount, relicInfo.id);
}

export async function withdrawFromRelic(reliquaryInfo: ReliquaryInfo, relicInfo: RelicInfo, amount: BigNumber) {
  return await reliquaryInfo.contract.withdraw(amount, relicInfo.id);
}

export async function createRelicAndDeposit(reliquaryInfo: ReliquaryInfo, poolId: number, amount: BigNumber) {
  return await reliquaryInfo.contract.createRelicAndDeposit(await reliquaryInfo.contract.signer.getAddress(), poolId, amount);
}

export async function getOppPriceUsd(signer: ethers.Signer) {
  const ethTokenPrice = await (async () => {
    const pairInfo = await getPairInformationByChain("optimism", VELODROMEV1_ETH_USDC_CONTRACT);
    return parseFloat(pairInfo.pair.priceUsd || "0");
  })();

  const oppContract = new ethers.Contract(OPP_CONTRACT, TOKEN_ABI, signer);
  const oppEthPairContract = new ethers.Contract(VELODROME_OPP_ETH_CONTRACT_V2, UNIV2_PAIR_ABI, signer);
  const reserves = await oppEthPairContract.getReserves();
  const ethLiquidity = reserves[0];
  const usdLiquidity = ethTokenPrice * parseFloat(ethers.utils.formatEther(ethLiquidity));
  const oppBalance = parseFloat(ethers.utils.formatEther(await oppContract.balanceOf(VELODROME_OPP_ETH_CONTRACT_V2)));
  return usdLiquidity / oppBalance;
}
