import { opChain } from "../../utils/chains";
import Notification from "../../components/Notification";

export default async function notificateTransaction(tx: any, txName: string) {
  const txUrl = opChain.blockExplorers?.default?.url + "/tx/" + tx.hash.toString();

  Notification({
    type: "",
    title: "Transaction Submitted",
    message: `Your ${txName} transaction was successfully submitted.`,
    link: txUrl,
  });

  const receipt = await tx.wait();

  Notification({
    type: "success",
    title: "Transaction Confirmed",
    message: `${txName} confirmed in block ${receipt.blockNumber}`,
    link: txUrl,
  });
}
