import { ethers } from "ethers";
import { VELODROME_VOTING_ESCROW_CONTRACT, VELODROME_VOTING_ESCROW_ABI } from "../utils/constants";

const veNFTs = [2647, 2648, 2649];

export async function queryVelodromeData(signer: ethers.Signer) {
  const contract = new ethers.Contract(VELODROME_VOTING_ESCROW_CONTRACT, VELODROME_VOTING_ESCROW_ABI, signer);

  let veVELO = ethers.utils.parseEther("0");
  let VELO = ethers.utils.parseEther("0");

  for (const veNFT of veNFTs) {
    const veVELOInVeNFT = await contract.balanceOfNFT(veNFT);
    const [VELOInVeNFT, _end] = await contract.locked(veNFT);
    veVELO = veVELO.add(veVELOInVeNFT);
    VELO = VELO.add(VELOInVeNFT);
  }

  return { veVelo: veVELO, velo: VELO };
}
