import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import Notification from "../components/Notification";
import { Block, ConfirmModal, ExternalLink, SwitchButton } from "../components/ui";
import { StateContext } from "../context/StateContext";
import { getReliquaryInfo, ReliquaryInfo } from "../contracts/reliquary";
import { AUTOBRIBES_EQUALIZER_CONTRACT, AUTOBRIBES_RETRO_CONTRACT, AUTOBRIBESV1_VELODROME_CONTRACT, AUTOBRIBESV2_VELODROME_CONTRACT } from "../utils/constants";
import { useLocalStorage } from "../utils/localStorage";
import Reliquary from "../components/Reliquary";

export default function Autobribes() {
  const {
    chain,
    signer,
  } = useContext(StateContext);

  const isConnected = chain && !chain.unsupported && signer;
  const [reliquaryInfo, setReliquaryInfo] = useState<ReliquaryInfo | undefined>(undefined);

  const [reliquaryVersion, setReliquaryVersion] = useState<number>(2);

  const getContract = (chainId: number, reliquaryVersion: number) => {
    switch(chainId) {
      case 10:
        return reliquaryVersion == 1 ? AUTOBRIBESV1_VELODROME_CONTRACT : AUTOBRIBESV2_VELODROME_CONTRACT;
      case 137:
        return AUTOBRIBES_RETRO_CONTRACT;
      case 250:
        return AUTOBRIBES_EQUALIZER_CONTRACT;
    }
    return undefined;
  }
  const getDex = (chainId: number, reliquaryVersion: number) => {
    switch(chainId) {
      case 10:
        return `Velodrome V${reliquaryVersion}`;
      case 137:
        return "Retro";
      case 250:
        return "Equalizer";
    }
    return undefined;
  }

  useEffect(() => {
    if (!isConnected) {
      return;
    }
    setReliquaryInfo(undefined);
    signer.getChainId().then((chainId) => {
      const contract = getContract(chainId, reliquaryVersion);
      if (contract === undefined) {
        Notification({
          type: "error",
          title: "Unsupported chain",
          message: `Please connect to a support chain`,
          link: ""
        });
        return;
      }
      getReliquaryInfo(signer, contract).then(setReliquaryInfo).catch((reason) => {
        Notification({
          type: "error",
          title: "Unable to fetch onchain data",
          message: `${reason}`,
          link: ""
        });
      });
    }).catch((reason) => {});
  }, [signer, isConnected, reliquaryVersion]);

  const reloadReliquaryInfo = useCallback(async () => {
    if (!isConnected) {
      return;
    }
    try {
      const contract = getContract(chain.id, reliquaryVersion);
      if (contract === undefined) {
        Notification({
          type: "error",
          title: "Unsupported chain",
          message: `Please connect to a support chain`,
          link: ""
        });
        return;
      }
      setReliquaryInfo(await getReliquaryInfo(signer, contract));
    } catch (e) {
      Notification({
        type: "error",
        title: "Unable to fetch onchain data",
        message: `${e}`,
        link: ""
      });
    }
  }, [chain, signer, isConnected, reliquaryVersion])

  const [warningAknowledged, setWarningAknowledged] = useLocalStorage("opp_autobribes-warning-aknowledged", false);
  const [warningAknowledgedTimestamp, setWarningAknowledgedTimestamp] = useLocalStorage("opp_autobribes-warning-aknowledged-timestamp", 0);

  useEffect(() => {
    const oneWeek = 1000 * 3600 * 24 * 7;
    if (Date.now() - warningAknowledgedTimestamp > oneWeek) {
      setWarningAknowledged(false);
    }
  }, [warningAknowledgedTimestamp, setWarningAknowledged]);

  return <>
    <div className="flex flex-col lg:max-w-[800px] xl:max-w-[1000px] justify-center items-center w-full font-roboto mb-4 xl:mb-0 lg:mb-0 gap-2">
      <Block>
          <p>Deposit LP tokens on the Autobribes protocol to earn real yield</p>
          <p>More information in <ExternalLink href="https://medium.com/optimism-prime/introducing-the-autobribes-protocol-6079b1bd6340">our Medium article</ExternalLink></p>
          <p>Summary in <ExternalLink href="https://twitter.com/Optimism_Pr/status/1666144553961660418">our Twitter thread</ExternalLink></p>
      </Block>
      {
        !isConnected || chain.id != 10 ? <></> :
        <Block>
          <div className="flex flex-row gap-2 w-full">
            <SwitchButton disabled={reliquaryVersion == 1} className="w-1/2" onClick={() => {setReliquaryVersion(1); setReliquaryInfo(undefined); }}>V1</SwitchButton>
            <SwitchButton disabled={reliquaryVersion == 2} className="w-1/2" onClick={() => {setReliquaryVersion(2); setReliquaryInfo(undefined); }}>V2</SwitchButton>
          </div>
        </Block>
      }
      {isConnected ?
        (reliquaryInfo !== undefined ?
          <Reliquary title={`Autobribes for ${getDex(chain.id, reliquaryVersion)}`} displayAllLevelsPerPool={false} reliquaryInfo={reliquaryInfo} reloadReliquaryInfo={reloadReliquaryInfo}/> : <Block><span><i className="fa fa-refresh fa-spin"></i> Loading</span></Block>
        ) : <Block>Please connect to Optimism</Block>
      }
    </div>
    <ConfirmModal isOpen={!warningAknowledged} onConfirm={() => {
      setWarningAknowledged(true);
      setWarningAknowledgedTimestamp(Date.now());
    }} buttonTitle="Accept">
      <div className="flex flex-col text-center text-xl text-red-500 gap-2">
        <p><i className="fa fa-2x fa-exclamation-triangle fa-beat"></i> DISCLAIMER <i className="fa fa-2x fa-exclamation-triangle fa-beat"></i></p>
        <p>The Autobribes protocol code is unaudited, use at your own risk.</p>
      </div>
    </ConfirmModal>
  </>
}
