import Link from "next/link";
import { useContext } from "react";
import { StateContext } from "../context/StateContext";

export default function Backgrounds() {
  const {
    setBackgroundIndex
  } = useContext(StateContext);
  return (
    <div className="grid lg:grid-cols-4 text-white gap-4 z-30 text-justify p-4">
      {
        Array.from(Array(44).keys()).map((key) => {
          const img = `${('000' + key).slice(-3)}`;
          return (
            <div key={key}>
              <img className="cursor-pointer" onClick={() => setBackgroundIndex(key)} src={`/banner/indexed/${img}.jpg`} alt={img} />
              <p className="bg-black/80 p-2"><Link href={`/banner/indexed/${img}.jpg`} passHref>
                <a target="_blank"><span className="underline">Background {key}</span> <i className="fa fa-external-link"></i></a>
              </Link></p>

            </div>
          )
        })
      }
    </div>
  )
}
