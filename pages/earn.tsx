import { useCallback, useContext, useEffect, useState } from "react";
import Notification from "../components/Notification";
import { Block, Button, ConfirmModal, ExternalLink, SwitchButton } from "../components/ui";
import { StateContext } from "../context/StateContext";
import { getReliquaryInfo, ReliquaryInfo } from "../contracts/reliquary";
import { RELIQUARY_PRIME_CONTRACT_V1, RELIQUARY_PRIME_CONTRACT_V2 } from "../utils/constants";
import { useLocalStorage } from "../utils/localStorage";
import Reliquary from "../components/Reliquary";

export default function Earn() {
  const {
    chain,
    signer,
  } = useContext(StateContext);

  const isConnected = chain && !chain.unsupported && signer && chain.id == 10;
  const [reliquaryInfo, setReliquaryInfo] = useState<ReliquaryInfo | undefined>(undefined);

  const [reliquaryVersion, setReliquaryVersion] = useState<number>(2);

  useEffect(() => {
    if (!isConnected) {
      return;
    }
    getReliquaryInfo(signer, reliquaryVersion == 1 ? RELIQUARY_PRIME_CONTRACT_V1 : RELIQUARY_PRIME_CONTRACT_V2).then(setReliquaryInfo).catch((reason) => {
      Notification({
        type: "error",
        title: "earn: getReliquaryInfo: Unable to fetch onchain data",
        message: `${reason}`,
        link: ""
      });
    });
  }, [signer, isConnected, reliquaryVersion]);

  const reloadReliquaryInfo = useCallback(async () => {
    if (!isConnected) {
      return;
    }
    try {
      setReliquaryInfo(await getReliquaryInfo(signer, reliquaryVersion == 1 ? RELIQUARY_PRIME_CONTRACT_V1 : RELIQUARY_PRIME_CONTRACT_V2));
    } catch (e) {
      Notification({
        type: "error",
        title: "Unable to fetch onchain data",
        message: `${e}`,
        link: ""
      });
    }
  }, [signer, isConnected, reliquaryVersion])

  const [warningAknowledged, setWarningAknowledged] = useLocalStorage("opp_earn-warning-aknowledged", false);
  const [warningAknowledgedTimestamp, setWarningAknowledgedTimestamp] = useLocalStorage("opp_earn-warning-aknowledged-timestamp", 0);

  useEffect(() => {
    const oneWeek = 1000 * 3600 * 24 * 7;
    if (Date.now() - warningAknowledgedTimestamp > oneWeek) {
      setWarningAknowledged(false);
    }
  }, [warningAknowledgedTimestamp, setWarningAknowledged]);

  return <>
    <div className="flex flex-col lg:max-w-[800px] xl:max-w-[1000px] justify-center items-center w-full font-roboto mb-4 xl:mb-0 lg:mb-0 gap-2">
      <Block>
          <p>Deposit your assets in the Reliquary Prime to earn a share of Optimism Prime treasury yield</p>
          <p>More information in <ExternalLink href="https://medium.com/optimism-prime/introducing-the-reliquary-prime-12d648212a07">our Medium article</ExternalLink></p>
      </Block>
      <Block>
        <div className="flex flex-row gap-2 w-full">
          <SwitchButton disabled={reliquaryVersion == 1} className="w-1/2" onClick={() => {setReliquaryVersion(1); setReliquaryInfo(undefined); }}>V1</SwitchButton>
          <SwitchButton disabled={reliquaryVersion == 2} className="w-1/2" onClick={() => {setReliquaryVersion(2); setReliquaryInfo(undefined); }}>V2</SwitchButton>
        </div>
      </Block>
      {isConnected ?
        (reliquaryInfo !== undefined ?
          <Reliquary title={`Reliquary Prime V${reliquaryVersion}`} displayAllLevelsPerPool={true} reliquaryInfo={reliquaryInfo} reloadReliquaryInfo={reloadReliquaryInfo}/> : <Block><span><i className="fa fa-refresh fa-spin"></i> Loading V{reliquaryVersion}</span></Block>
        ) : <Block>Please connect to Optimism</Block>
      }
    </div>
    <ConfirmModal isOpen={!warningAknowledged} onConfirm={() => {
      setWarningAknowledged(true);
      setWarningAknowledgedTimestamp(Date.now());
    }} buttonTitle="Accept">
      <div className="flex flex-col text-center text-xl text-red-500 gap-2">
        <p><i className="fa fa-2x fa-exclamation-triangle fa-beat"></i> DISCLAIMER <i className="fa fa-2x fa-exclamation-triangle fa-beat"></i></p>
        <p>The Reliquary Prime code is unaudited, use at your own risk.</p>
      </div>
    </ConfirmModal>
  </>
}
