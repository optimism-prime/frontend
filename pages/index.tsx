import { useCallback, useEffect, useState, useContext } from 'react';
import { getPairInformationByChain } from 'dexscreener-api';
import { StateContext } from '../context/StateContext';
import { queryVelodromeData } from '../contracts/treasury';

import { ethers } from 'ethers';
import { VELODROME_OPP_ETH_CONTRACT_V2 } from '../utils/constants';
import { getOppPriceUsd } from '../contracts/reliquary';

const Round = ({
  text,
  children,
}: {
  text: string;
  children?: React.ReactNode;
}) => {
  return (
    <div className="flex flex-col justify-center items-center">
      <span className="text-black">{text}</span>
      {children}
    </div>
  );
};

const roundStyles = () => {
  return 'flex flex-col justify-center items-center hover:scale-105 transition ease-in-out gap-6 p-6 bg-[#bb292d] w-[300px] h-[300px] sm:w-[400px] sm:h-[400px] 2xl:w-[400px] 2xl:h-[400px] rounded-full shadow-[0_1px_135px_rgba(255,255,255)]';
};

const Home = () => {
  const { isLoading, setIsLoading } = useContext(StateContext);
  const [stats, setStats] = useState<any>(null);
  const [numberVe, setNumberVe] = useState('');
  const { chain, signer } = useContext(StateContext);

  const getStats = useCallback(async () => {
    setIsLoading(true);
    try {
      const pairsResponse = await getPairInformationByChain(
        'optimism',
        VELODROME_OPP_ETH_CONTRACT_V2
      );
      setStats(pairsResponse.pair);
    } catch (e) {
      if (signer) {
        const oppPriceUsd = await getOppPriceUsd(signer);
        setStats({priceUsd: oppPriceUsd.toFixed(7).toString()});
      }
    }
    setIsLoading(false);
  }, [setStats, setIsLoading, signer]);

  useEffect(() => {
    getStats();
  }, [getStats]);

  useEffect(() => {
    if (chain && !chain.unsupported && signer) {
      queryVelodromeData(signer).then((response) => {
        const result = response.velo.div(ethers.utils.parseUnits('1', 16));
        setNumberVe(ethers.utils.commify(ethers.utils.formatUnits(result, 2)));
      });
    }
  }, [chain, signer]);

  return (
    <div className="home-page w-full flex justify-center">
      <div className="flex justify-evenly items-center h-full w-full max-w-[1920px] 2xl:min-h-[100%]  rounded-3xl p-6 py-[3.8rem] font-transformers bg-transparent text-white xl:mt-8 flex-wrap text-3xl">
        <div className={roundStyles()}>
          <Round text="MKT CAP">
            <p className="xl:text-5xl">
              {!stats || !stats?.fdv || isLoading
                ? '---'
                : `${stats?.fdv?.toLocaleString('en-us')}`}
            </p>
          </Round>
          <Round text="VOLUME">
            <p className="xl:text-5xl">
              {!stats || !stats?.volume || isLoading
                ? '---'
                : `${stats?.volume?.h24?.toLocaleString('en-us')}`}
            </p>
          </Round>
        </div>

        <div className={roundStyles()}>
          <Round text="USD PRICE">
            <p className="xl:text-5xl">
              {!stats || !stats?.priceUsd || isLoading
                ? '---'
                : `${stats?.priceUsd?.toLocaleString('en-us')}`}
            </p>
          </Round>
          <Round text=" LOCKED VELO">
            <p className="xl:text-5xl">{numberVe}</p>
          </Round>
        </div>
      </div>
    </div>
  );
};

export default Home;
