import { ExternalLink } from "../components/ui";

export default function Opp() {
  return (
    <div className="flex flex-col lg:max-w-[800px] xl:max-w-[1000px] 2xl:max-w-[1920px] justify-center items-center w-full font-roboto-cond mb-4 xl:mb-0 lg:mb-0">
    {/* <div className="flex flex-col max-w-[1920px] flex-1 h-full py-0 px-12"> */}
      {/* TEXT */}
      <div className="flex flex-col text-white gap-4 z-30 text-justify bg-black bg-opacity-80 px-10 pb-10 shadow-[1px_5px_30px_rgba(255,255,255)]">
        <p className="text-3xl md:pt-6 leading-relaxed">
          <span className="font-transformers text-4xl text-white/80">
            OptiPrimus
          </span>
          , the creator god - the multiversal force for good has provided some
          of his life force to bring Optimism Prime to life.
        </p>
        <div className="text-2xl space-y-8">
          <p>
            Even in the face of losing decentralization to the combined might of
            the Regulator forces, Optimism Prime never abandoned his fight to
            liberate the Optimistic Chain. Given the current events and
            regulations, Optimism Prime and the decentrabots have staged a plan
            to counter this unfair legislators: the <ExternalLink href="https://optimistic.etherscan.io/token/0x676f784d19c7f1ac6c6beaeaac78b02a73427852">$OPP</ExternalLink> token.
          </p>

          <p>
            Optimism Prime is the first <span className="underline">FAIR LAUNCH</span> token
            mechanics in the Optimism Chain. NO PRESALE, NO VC's. Just a fair
            token, cheap gas and lots of fun!
          </p>
          <p>
            The token has a 1,000,000,000 supply and is fully distributed. It can be bought and used for liquidity providing on <ExternalLink href="https://app.velodrome.finance/swap?from=eth&to=0x676f784d19c7f1ac6c6beaeaac78b02a73427852">Velodrome</ExternalLink>. A leveraged farming pools is available on <ExternalLink href="https://www.tarot.to/">Tarot</ExternalLink>.
          </p>
          <p>
            We have suffered losses, but we've not lost the war, we can still
            have lots of fun, and most important, we can still do it FREELY on
            Optimism.
            Hop on our journey and join our socials. <ExternalLink href="https://discord.gg/SpfdM2VwUy">Be a part of the first
            legit meme token and let's save the blockchain together.</ExternalLink>
          </p>
          <p>
            <span className="font-transformers text-white/80">
              Now all we need is a little Energon and a lot of Luck.
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}
