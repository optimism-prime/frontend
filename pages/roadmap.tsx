import { Block, ExternalLink } from "../components/ui";

export default function Opp() {
  return (
    <div className="flex flex-col gap-2">
      <Block className="text-2xl pl-16 pr-16">
        <h1 className="text-3xl font-transformers">Optimism Prime</h1>
        <ul className="list-disc">
          <li>Grow our treasury</li>
          <li>Automate our treasury management workflow</li>
        </ul>
      </Block>
      <Block className="text-2xl pl-16 pr-16">
        <p className="text-3xl font-transformers">Reliquary Prime & Autobribes</p>
        <ul className="list-disc">
          <li>Improve UI (claim all, filter / sort relics, merge / split / send)</li>
          <li>Write documentation</li>
          <li>Integrate other ve(3,3) exchanges</li>
          <li>Beta test Autobribes using funds from Optimism Prime Treasury</li>
          <li>Grow a veNFT for each instance</li>
          <li>Have the code reviewed by pairs and find funding for audits</li>
          <li>Clarify fundraising & incentive model and find seed investors</li>
        </ul>
      </Block>
      <Block className="text-2xl pl-16 pr-16">
      <p className="text-3xl font-transformers">Lenderbots DAO</p>
        <ul className="list-disc">
          <li>Deploy staking mechanism to distribute interest rate</li>
        </ul>
      </Block>
      <Block className="text-2xl pl-16 pr-16">
      <p className="text-3xl font-transformers">Autobots NFTs</p>
        <ul className="list-disc">
          <li>Automate reward distribution</li>
          <li>Relaunch collection with new designs & royalties</li>
          <li>Add some small revenue sharing to common Autobots</li>
        </ul>
      </Block>
    </div>
  );
}
