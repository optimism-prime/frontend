from pathlib import Path

from PIL import Image

images = [str(filepath) for filepath in Path("./upscaled_png").iterdir() if filepath.suffix in [".png"]]

for image_path in images:
    image = Image.open(image_path)
    image.save(Path("indexed") / f"{Path(image_path).stem}.jpg", quality=100, subsampling=0)
