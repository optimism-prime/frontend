from pathlib import Path

from PIL import Image

images = [str(filepath) for filepath in Path(".").iterdir() if filepath.suffix in [".png", ".jpg"]]

for idx, image_path in enumerate(images):
    image = Image.open(image_path)
    image.save(Path("indexed") / f"{idx:03}.jpg")
