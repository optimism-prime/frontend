from PIL import Image


def split_image(image_path):
    # Open the image
    image = Image.open(image_path)

    # Get the dimensions of the image
    width, height = image.size

    # Calculate the width and height for each split image
    split_width = width // 2
    split_height = height // 2

    # Split the image into four parts
    top_left = image.crop((0, 0, split_width, split_height))
    top_right = image.crop((split_width, 0, width, split_height))
    bottom_left = image.crop((0, split_height, split_width, height))
    bottom_right = image.crop((split_width, split_height, width, height))

    # Return the four split images
    return top_left, top_right, bottom_left, bottom_right

images = [
    "Arne_abstract_background_with_car_engine_parts_hyper-realistic__3d4d0ee6-9409-41f1-93c9-2b7010bca320.png",
    "Arne_abstract_background_with_car_engine_parts_hyper-realistic__4d853206-374f-4846-9d16-2a60d5dc3ee3.png",
    "Arne_abstract_background_with_car_engine_parts_hyper-realistic__357fee1b-886c-4935-a83f-4f4bb0cadc99.png",
    "Arne_abstract_background_with_car_engine_parts_hyper-realistic__f06d9163-60b6-42c3-84c1-f5e0cdccf62f.png",
    "Arne_abstract_background_with_engine_parts_hyper-realistic_sci-_58e160f2-e9e3-4a98-8445-16e612ed0846.png",
    "Arne_abstract_background_with_gears_circuits_and_springs_hyper-_74abb865-6e57-49db-89c7-a8c9ae6f7d64.webp",
    "Arne_catroon_background_with_car_engine_parts_sci-fi_black_red__6d091faa-81c5-4c7e-aad0-b31994564d78.png",
    "Arne_sketch_background_with_car_engine_parts_sci-fi_black_red_a_40e50f6c-d187-4761-b1c5-a0e06e4dd5f3.webp",
    "Arne_sketch_background_with_car_engine_parts_sci-fi_black_red_a_83ab0562-cbf3-4358-8e91-32f1ae66cd2e.webp",
    "Arne_sketch_background_with_car_engine_parts_sci-fi_black_red_a_2257cffd-689d-4d27-ba88-9479a5ae610b.webp",
]

# Path to the image you want to split
for image_path in images:
    # Split the image
    top_left, top_right, bottom_left, bottom_right = split_image(image_path)

    # Save the split images
    top_left.save(f"{image_path.split('.')[0]}_top_left.jpg")
    top_right.save(f"{image_path.split('.')[0]}_top_right.jpg")
    bottom_left.save(f"{image_path.split('.')[0]}_bottom_left.jpg")
    bottom_right.save(f"{image_path.split('.')[0]}_bottom_right.jpg")
