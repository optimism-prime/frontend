import type { Chain } from "@rainbow-me/rainbowkit";
export const opChain: Chain & {
  unsupported?: boolean | undefined;
} = {
  id: 10,
  name: "Optimism",
  network: "Optimism",
  iconUrl: "https://assets.coingecko.com/coins/images/25244/small/Optimism.png?1660904599",
  iconBackground: "#fff",
  nativeCurrency: {
    decimals: 18,
    name: "Ethereum",
    symbol: "ETH",
  },
  rpcUrls: {
    default: "https://mainnet.optimism.io",
  },
  blockExplorers: {
    default: { name: "Optimism", url: "https://optimistic.etherscan.io" },
  },
  testnet: false,
  unsupported: false,
};

export const ftmChain: Chain & {
  unsupported?: boolean | undefined;
} = {
  id: 250,
  name: "Fantom",
  network: "Fantom",
  iconUrl: "https://assets.coingecko.com/coins/images/4001/small/Fantom_round.png?1669652346",
  iconBackground: "#fff",
  nativeCurrency: {
    decimals: 18,
    name: "Fantom",
    symbol: "FTM",
  },
  rpcUrls: {
    default: "https://rpc.ankr.com/fantom",
  },
  blockExplorers: {
    default: { name: "Fantom", url: "https://ftmscan.com" },
  },
  testnet: false,
  unsupported: false,
};

export const polygonChain: Chain & {
  unsupported?: boolean | undefined;
} = {
  id: 137,
  name: "Polygon",
  network: "Polygon",
  iconUrl: "https://assets.coingecko.com/coins/images/4713/small/matic-token-icon.png?1624446912",
  iconBackground: "#fff",
  nativeCurrency: {
    decimals: 18,
    name: "Matic",
    symbol: "MATIC",
  },
  rpcUrls: {
    default: "https://rpc.ankr.com/polygon",
  },
  blockExplorers: {
    default: { name: "Polygon", url: "https://polygonscan.com" },
  },
  testnet: false,
  unsupported: false,
};
