export enum ButtonType {
  primary,
  secondary,
}

export const enum DecimalsType {
  zero = 0,
  two = 2,
  six = 6,
}

export const enum ChartDays {
  seven = 7,
  fifteen = 15,
  thirdy = 30,
}
export const enum ChartType {
  Minted = "1USD Minted",
  Staked = "1USD Staked",
  APY = "APY",
  Distribution = "Distribution",
}

export const enum Theme {
  light = "light",
  dark = "dark",
}

export const enum NFTAmount {
  one = "one",
  three = "three",
  five = "five",
}

export const enum Page {
  dashboard,
  opp,
  roadmap,
  stake,
  nft,
  earn,
  autobribes,
  backgrounds,
}
