import { useEffect, useState } from "react";

const getLocalStorageValue = <T>(key: string, fallbackValue: T) => {
  if (typeof window === "undefined") {
    return fallbackValue;
  }
  const stored = localStorage.getItem(key);
  return stored ? JSON.parse(stored) : fallbackValue;
};

export function useLocalStorage<T>(key: string, fallbackValue: T) {
  const [value, setValue] = useState(getLocalStorageValue(key, fallbackValue));
  useEffect(() => {
    setValue(getLocalStorageValue(key, fallbackValue));
  }, [fallbackValue, key]);

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue] as const;
}
